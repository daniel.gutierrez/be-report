<?php

namespace App\Framework\Controller;

use App\Domain\Exception\ValidationException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class AbstractController extends Controller
{

    /**
     * @param $entity
     * @throws ValidationException
     */
    protected function validateRequest($entity)
    {
        $validator = $this->get('validator');

        $errors = $validator->validate($entity);

        if (\count($errors)) {
            throw new ValidationException(iterator_to_array($errors));
        }

    }
}
